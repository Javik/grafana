######################################
#           _             _ _        #
#          | |           (_) |       #
#          | | __ ___   ___| | __    #
#      _   | |/ _` \ \ / / | |/ /    #
#     | |__| | (_| |\ V /| |   <     #
#      \____/ \__,_| \_/ |_|_|\_\    #
#                                    #
######################################

####
## Public User keys
####

locals {
  key_lines = toset(compact((split("\n", file("./public-key.txt")))))
}

resource "hcloud_ssh_key" "user_ssh" {
  for_each   = local.key_lines
  name       = split(";", each.key)[0]
  public_key = split(";", each.key)[1]

  labels = {
    type      = "user_ssh"
    terraform = true
  }
}

####
## Terraform SSH Key
####

resource "tls_private_key" "terraform_ssh" {
  algorithm = "ED25519"
}

resource "hcloud_ssh_key" "terraform_ssh" {
  name       = "Terraform SSH"
  public_key = tls_private_key.terraform_ssh.public_key_openssh

  labels = {
    type      = "terraform_ssh"
    terraform = true
  }
}
