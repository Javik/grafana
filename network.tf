######################################
#           _             _ _        #
#          | |           (_) |       #
#          | | __ ___   ___| | __    #
#      _   | |/ _` \ \ / / | |/ /    #
#     | |__| | (_| |\ V /| |   <     #
#      \____/ \__,_| \_/ |_|_|\_\    #
#                                    #
######################################

####
## Grafana
####

resource "hcloud_network" "grafana" {
  name     = "Grafana-Network"
  ip_range = var.grafana_network_ip_range

  labels = {
    service   = "grafana_network",
    terraform = true
  }
}

resource "hcloud_network_subnet" "grafana_subnet" {
  network_id   = hcloud_network.grafana.id
  type         = "cloud"
  network_zone = "eu-central"
  ip_range     = var.grafana_network_ip_range
}
