######################################
#           _             _ _        #
#          | |           (_) |       #
#          | | __ ___   ___| | __    #
#      _   | |/ _` \ \ / / | |/ /    #
#     | |__| | (_| |\ V /| |   <     #
#      \____/ \__,_| \_/ |_|_|\_\    #
#                                    #
######################################

/*locals {
  puppetmaster_ipv4 = length(hcloud_floating_ip.puppetmaster_floating_ipv4) < 1 ? "${hcloud_primary_ip.puppetmaster_primary_ipv4[0].ip_address}" : "${hcloud_floating_ip.puppetmaster_floating_ipv4[0].ip_address}"
  puppetmaster_ipv6 = length(hcloud_floating_ip.puppetmaster_floating_ipv6) < 1 ? "${hcloud_primary_ip.puppetmaster_primary_ipv6[0].ip_address}" : "${hcloud_floating_ip.puppetmaster_floating_ipv6[0].ip_address}1"
  #  webserver_ipv4    = length(hcloud_floating_ip.webserver_floating_ipv4) < 1 ? "${hcloud_primary_ip.webserver_primary_ipv4[0].ip_address}" : "${hcloud_floating_ip.webserver_floating_ipv4[0].ip_address}"
  webserver_ipv6  = length(hcloud_floating_ip.webserver_floating_ipv6) < 1 ? "${hcloud_primary_ip.webserver_primary_ipv6[0].ip_address}" : "${hcloud_floating_ip.webserver_floating_ipv6[0].ip_address}1"
  mailserver_ipv4 = length(hcloud_floating_ip.mailserver_floating_ipv4) < 1 ? "${hcloud_primary_ip.mailserver_primary_ipv4[0].ip_address}" : "${hcloud_floating_ip.mailserver_floating_ipv4[0].ip_address}"
  mailserver_ipv6 = length(hcloud_floating_ip.mailserver_floating_ipv6) < 1 ? "${hcloud_primary_ip.mailserver_primary_ipv6[0].ip_address}" : "${hcloud_floating_ip.mailserver_floating_ipv6[0].ip_address}1"
}*/
