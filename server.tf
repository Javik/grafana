######################################
#           _             _ _        #
#          | |           (_) |       #
#          | | __ ___   ___| | __    #
#      _   | |/ _` \ \ / / | |/ /    #
#     | |__| | (_| |\ V /| |   <     #
#      \____/ \__,_| \_/ |_|_|\_\    #
#                                    #
######################################      

resource "hcloud_server" "grafana_frontend" {
  count                    = var.grafana_frontend_count
  shutdown_before_deletion = true

  name = format("%s-%s.%s",
    "grafana",
    (count.index % 2 == 0 ? "running" : "broken"),
    "mondbasis24.de"
  )

  image       = "debian-12"
  server_type = "cx11"
  location    = "fsn1"

  labels = {
    service   = "grafana_frontend"
    terraform = true
  }
  public_net {
    ipv4 = hcloud_primary_ip.grafana_frontend_primary_ipv4[count.index].id
    ipv6 = hcloud_primary_ip.grafana_frontend_primary_ipv6[count.index].id
  }

  ssh_keys = concat(
    [for key in hcloud_ssh_key.user_ssh : key.name],
    [hcloud_ssh_key.terraform_ssh.name]
  )

  connection {
    type        = "ssh"
    user        = "root"
    private_key = tls_private_key.terraform_ssh.private_key_openssh
    host        = self.ipv4_address
  }

  provisioner "file" {
    content     = tls_private_key.terraform_ssh.private_key_openssh
    destination = "/root/.ssh/terraform_key"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod --verbose 600 /root/.ssh/terraform_key",
    ]
  }
}

resource "hcloud_server_network" "grafana_frontend_network" {
  count      = length(hcloud_server.grafana_frontend)
  server_id  = hcloud_server.grafana_frontend[count.index].id
  network_id = hcloud_network.grafana.id
}


resource "hcloud_server" "grafana_storage" {
  count                    = var.grafana_storage_count
  shutdown_before_deletion = true

  name = format("%s-%s.%s",
    "grafana-storage",
    (count.index % 2 == 0 ? "running" : "broken"),
    "mondbasis24.de"
  )

  image       = "debian-12"
  server_type = "cx11"
  location    = "fsn1"

  labels = {
    service   = "grafana_storage"
    terraform = true
  }
  public_net {
    ipv4 = hcloud_primary_ip.grafana_storage_primary_ipv4[count.index].id
    ipv6 = hcloud_primary_ip.grafana_storage_primary_ipv6[count.index].id
  }

  ssh_keys = concat(
    [for key in hcloud_ssh_key.user_ssh : key.name],
    [hcloud_ssh_key.terraform_ssh.name]
  )

  connection {
    type        = "ssh"
    user        = "root"
    private_key = tls_private_key.terraform_ssh.private_key_openssh
    host        = self.ipv4_address
  }

  provisioner "file" {
    content     = tls_private_key.terraform_ssh.private_key_openssh
    destination = "/root/.ssh/terraform_key"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod --verbose 600 /root/.ssh/terraform_key",
    ]
  }
}

resource "hcloud_server_network" "grafana_storage_network" {
  count      = length(hcloud_server.grafana_storage)
  server_id  = hcloud_server.grafana_storage[count.index].id
  network_id = hcloud_network.grafana.id
}

resource "hcloud_server" "grafana_db" {
  count                    = var.grafana_db_count
  shutdown_before_deletion = true

  name = format("%s-%s.%s",
    "grafana-db",
    (count.index % 2 == 0 ? "running" : "broken"),
    "mondbasis24.de"
  )

  image       = "debian-12"
  server_type = "cx11"
  location    = "fsn1"

  labels = {
    service   = "grafana_storage"
    terraform = true
  }
  public_net {
    ipv4 = hcloud_primary_ip.grafana_db_primary_ipv4[count.index].id
    ipv6 = hcloud_primary_ip.grafana_db_primary_ipv6[count.index].id
  }

  ssh_keys = concat(
    [for key in hcloud_ssh_key.user_ssh : key.name],
    [hcloud_ssh_key.terraform_ssh.name]
  )

  connection {
    type        = "ssh"
    user        = "root"
    private_key = tls_private_key.terraform_ssh.private_key_openssh
    host        = self.ipv4_address
  }

  provisioner "file" {
    content     = tls_private_key.terraform_ssh.private_key_openssh
    destination = "/root/.ssh/terraform_key"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod --verbose 600 /root/.ssh/terraform_key",
    ]
  }
}

resource "hcloud_server_network" "grafana_db_network" {
  count      = length(hcloud_server.grafana_db)
  server_id  = hcloud_server.grafana_db[count.index].id
  network_id = hcloud_network.grafana.id
}

resource "hcloud_server" "webserver" {
  shutdown_before_deletion = true

  name = format("%s.%s",
    "web",
    "mondbasis24.de"
  )

  image       = "debian-12"
  server_type = "cx11"
  location    = "fsn1"

  labels = {
    service   = "grafana_storage"
    terraform = true
  }
  public_net {
    ipv4 = hcloud_primary_ip.webserver_primary_ipv4.id
    ipv6 = hcloud_primary_ip.webserver_primary_ipv6.id
  }

  ssh_keys = concat(
    [for key in hcloud_ssh_key.user_ssh : key.name],
    [hcloud_ssh_key.terraform_ssh.name]
  )

  connection {
    type        = "ssh"
    user        = "root"
    private_key = tls_private_key.terraform_ssh.private_key_openssh
    host        = self.ipv4_address
  }

  provisioner "file" {
    content     = tls_private_key.terraform_ssh.private_key_openssh
    destination = "/root/.ssh/terraform_key"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod --verbose 600 /root/.ssh/terraform_key",
    ]
  }
}

resource "hcloud_server_network" "webserver_network" {
  server_id  = hcloud_server.webserver.id
  network_id = hcloud_network.grafana.id
}

