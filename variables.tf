######################################
#           _             _ _        #
#          | |           (_) |       #
#          | | __ ___   ___| | __    #
#      _   | |/ _` \ \ / / | |/ /    #
#     | |__| | (_| |\ V /| |   <     #
#      \____/ \__,_| \_/ |_|_|\_\    #
#                                    #
######################################

variable "cloudflare_zone_id" {
  type      = string
  sensitive = true
}

variable "cloudflare_default_ttl" {
  type      = number
  sensitive = false
  default   = 3600
}

variable "grafana_frontend_count" {
  default   = 2
  type      = number
  sensitive = false
}

variable "grafana_storage_count" {
  default   = 2
  type      = number
  sensitive = false
}

variable "grafana_db_count" {
  default   = 2
  type      = number
  sensitive = false
}

variable "grafana_network_ip_range" {
  default   = "10.120.0.0/24"
  type      = string
  sensitive = false
}
