######################################
#           _             _ _        #
#          | |           (_) |       #
#          | | __ ___   ___| | __    #
#      _   | |/ _` \ \ / / | |/ /    #
#     | |__| | (_| |\ V /| |   <     #
#      \____/ \__,_| \_/ |_|_|\_\    #
#                                    #
######################################

####
## Grafana FrontEnd
####

resource "hcloud_primary_ip" "grafana_frontend_primary_ipv4" {
  count = var.grafana_frontend_count

  name = format("%s-%s.%s-%s",
    "grafana",
    (count.index % 2 == 0 ? "running" : "broken"),
    "mondbasis24.de",
    "ipv4"
  )

  datacenter    = "fsn1-dc14"
  type          = "ipv4"
  assignee_type = "server"
  auto_delete   = false
  labels = {
    service   = "ipv4",
    terraform = true
  }
}

resource "hcloud_primary_ip" "grafana_frontend_primary_ipv6" {
  count = var.grafana_frontend_count

  name = format("%s-%s.%s-%s",
    "grafana",
    (count.index % 2 == 0 ? "running" : "broken"),
    "mondbasis24.de",
    "ipv6"
  )

  datacenter    = "fsn1-dc14"
  type          = "ipv6"
  assignee_type = "server"
  auto_delete   = false
  labels = {
    service   = "ipv6",
    terraform = true
  }
}

resource "hcloud_primary_ip" "grafana_storage_primary_ipv4" {
  count = var.grafana_frontend_count

  name = format("%s-%s.%s-%s",
    "grafana-storage",
    (count.index % 2 == 0 ? "running" : "broken"),
    "mondbasis24.de",
    "ipv4"
  )

  datacenter    = "fsn1-dc14"
  type          = "ipv4"
  assignee_type = "server"
  auto_delete   = false
  labels = {
    service   = "ipv4",
    terraform = true
  }
}

resource "hcloud_primary_ip" "grafana_storage_primary_ipv6" {
  count = var.grafana_frontend_count

  name = format("%s-%s.%s-%s",
    "grafana-storage",
    (count.index % 2 == 0 ? "running" : "broken"),
    "mondbasis24.de",
    "ipv6"
  )

  datacenter    = "fsn1-dc14"
  type          = "ipv6"
  assignee_type = "server"
  auto_delete   = false
  labels = {
    service   = "ipv6",
    terraform = true
  }
}

resource "hcloud_primary_ip" "grafana_db_primary_ipv4" {
  count = var.grafana_frontend_count

  name = format("%s-%s.%s-%s",
    "grafana-db",
    (count.index % 2 == 0 ? "running" : "broken"),
    "mondbasis24.de",
    "ipv4"
  )

  datacenter    = "fsn1-dc14"
  type          = "ipv4"
  assignee_type = "server"
  auto_delete   = false
  labels = {
    service   = "ipv4",
    terraform = true
  }
}

resource "hcloud_primary_ip" "grafana_db_primary_ipv6" {
  count = var.grafana_frontend_count

  name = format("%s-%s.%s-%s",
    "grafana-db",
    (count.index % 2 == 0 ? "running" : "broken"),
    "mondbasis24.de",
    "ipv6"
  )

  datacenter    = "fsn1-dc14"
  type          = "ipv6"
  assignee_type = "server"
  auto_delete   = false
  labels = {
    service   = "ipv6",
    terraform = true
  }
}

resource "hcloud_primary_ip" "webserver_primary_ipv4" {
  name = format("%s.%s-%s",
    "web",
    "mondbasis24.de",
    "ipv4"
  )

  datacenter    = "fsn1-dc14"
  type          = "ipv4"
  assignee_type = "server"
  auto_delete   = false
  labels = {
    service   = "ipv4",
    terraform = true
  }
}

resource "hcloud_primary_ip" "webserver_primary_ipv6" {
  name = format("%s.%s-%s",
    "web",
    "mondbasis24.de",
    "ipv6"
  )

  datacenter    = "fsn1-dc14"
  type          = "ipv6"
  assignee_type = "server"
  auto_delete   = false
  labels = {
    service   = "ipv6",
    terraform = true
  }
}
