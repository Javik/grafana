######################################
#           _             _ _        #
#          | |           (_) |       #
#          | | __ ___   ___| | __    #
#      _   | |/ _` \ \ / / | |/ /    #
#     | |__| | (_| |\ V /| |   <     #
#      \____/ \__,_| \_/ |_|_|\_\    #
#                                    #
######################################

####
## Grafana Frontend
####

## DNS

resource "cloudflare_record" "grafana_frontend_ipv4" {
  count           = var.grafana_frontend_count
  allow_overwrite = true
  zone_id         = var.cloudflare_zone_id
  name            = hcloud_server.grafana_frontend[count.index].name
  value           = hcloud_primary_ip.grafana_frontend_primary_ipv4[count.index].ip_address
  type            = "A"
  ttl             = var.cloudflare_default_ttl
}
resource "cloudflare_record" "grafana_frontend_ipv6" {
  count           = var.grafana_frontend_count
  allow_overwrite = true
  zone_id         = var.cloudflare_zone_id
  name            = hcloud_server.grafana_frontend[count.index].name
  value           = "${hcloud_primary_ip.grafana_frontend_primary_ipv6[count.index].ip_address}1"
  type            = "AAAA"
  ttl             = var.cloudflare_default_ttl
}

## rDNS

resource "hcloud_rdns" "grafana_frontend_rdns_ipv4" {
  count         = var.grafana_frontend_count
  primary_ip_id = hcloud_primary_ip.grafana_frontend_primary_ipv4[count.index].id
  ip_address    = hcloud_primary_ip.grafana_frontend_primary_ipv4[count.index].ip_address
  dns_ptr       = hcloud_server.grafana_frontend[count.index].name
}
resource "hcloud_rdns" "grafana_frontend_rdns_ipv6" {
  count         = var.grafana_frontend_count
  primary_ip_id = hcloud_primary_ip.grafana_frontend_primary_ipv6[count.index].id
  ip_address    = hcloud_primary_ip.grafana_frontend_primary_ipv6[count.index].ip_address
  dns_ptr       = hcloud_server.grafana_frontend[count.index].name
}

####
## Grafana Storage
####

## DNS

resource "cloudflare_record" "grafana_storage_ipv4" {
  count           = var.grafana_storage_count
  allow_overwrite = true
  zone_id         = var.cloudflare_zone_id
  name            = hcloud_server.grafana_storage[count.index].name
  value           = hcloud_primary_ip.grafana_storage_primary_ipv4[count.index].ip_address
  type            = "A"
  ttl             = var.cloudflare_default_ttl
}
resource "cloudflare_record" "grafana_storage_ipv6" {
  count           = var.grafana_storage_count
  allow_overwrite = true
  zone_id         = var.cloudflare_zone_id
  name            = hcloud_server.grafana_storage[count.index].name
  value           = "${hcloud_primary_ip.grafana_storage_primary_ipv6[count.index].ip_address}1"
  type            = "AAAA"
  ttl             = var.cloudflare_default_ttl
}

## rDNS

resource "hcloud_rdns" "grafana_storage_rdns_ipv4" {
  count         = var.grafana_storage_count
  primary_ip_id = hcloud_primary_ip.grafana_storage_primary_ipv4[count.index].id
  ip_address    = hcloud_primary_ip.grafana_storage_primary_ipv4[count.index].ip_address
  dns_ptr       = hcloud_server.grafana_storage[count.index].name
}
resource "hcloud_rdns" "grafana_storage_rdns_ipv6" {
  count         = var.grafana_storage_count
  primary_ip_id = hcloud_primary_ip.grafana_storage_primary_ipv6[count.index].id
  ip_address    = hcloud_primary_ip.grafana_storage_primary_ipv6[count.index].ip_address
  dns_ptr       = hcloud_server.grafana_storage[count.index].name
}

####
## Grafana DB
####

## DNS

resource "cloudflare_record" "grafana_db_ipv4" {
  count           = var.grafana_db_count
  allow_overwrite = true
  zone_id         = var.cloudflare_zone_id
  name            = hcloud_server.grafana_db[count.index].name
  value           = hcloud_primary_ip.grafana_db_primary_ipv4[count.index].ip_address
  type            = "A"
  ttl             = var.cloudflare_default_ttl
}
resource "cloudflare_record" "grafana_db_ipv6" {
  count           = var.grafana_db_count
  allow_overwrite = true
  zone_id         = var.cloudflare_zone_id
  name            = hcloud_server.grafana_db[count.index].name
  value           = "${hcloud_primary_ip.grafana_db_primary_ipv6[count.index].ip_address}1"
  type            = "AAAA"
  ttl             = var.cloudflare_default_ttl
}

## rDNS

resource "hcloud_rdns" "grafana_db_rdns_ipv4" {
  count         = var.grafana_db_count
  primary_ip_id = hcloud_primary_ip.grafana_db_primary_ipv4[count.index].id
  ip_address    = hcloud_primary_ip.grafana_db_primary_ipv4[count.index].ip_address
  dns_ptr       = hcloud_server.grafana_db[count.index].name
}
resource "hcloud_rdns" "grafana_db_rdns_ipv6" {
  count         = var.grafana_db_count
  primary_ip_id = hcloud_primary_ip.grafana_db_primary_ipv6[count.index].id
  ip_address    = hcloud_primary_ip.grafana_db_primary_ipv6[count.index].ip_address
  dns_ptr       = hcloud_server.grafana_db[count.index].name
}

####
## Webserver
####

## DNS

resource "cloudflare_record" "webserver_ipv4" {
  allow_overwrite = true
  zone_id         = var.cloudflare_zone_id
  name            = hcloud_server.webserver.name
  value           = hcloud_primary_ip.webserver_primary_ipv4.ip_address
  type            = "A"
  ttl             = var.cloudflare_default_ttl
}
resource "cloudflare_record" "webserver_ipv6" {
  allow_overwrite = true
  zone_id         = var.cloudflare_zone_id
  name            = hcloud_server.webserver.name
  value           = "${hcloud_primary_ip.webserver_primary_ipv6.ip_address}1"
  type            = "AAAA"
  ttl             = var.cloudflare_default_ttl
}

## rDNS

resource "hcloud_rdns" "webserver_rdns_ipv4" {
  primary_ip_id = hcloud_primary_ip.webserver_primary_ipv4.id
  ip_address    = hcloud_primary_ip.webserver_primary_ipv4.ip_address
  dns_ptr       = hcloud_server.webserver.name
}
resource "hcloud_rdns" "webserver_rdns_ipv6" {
  primary_ip_id = hcloud_primary_ip.webserver_primary_ipv6.id
  ip_address    = hcloud_primary_ip.webserver_primary_ipv6.ip_address
  dns_ptr       = hcloud_server.webserver.name
}

# Domain DNS

resource "cloudflare_record" "mondbasis24de_ipv4" {
  allow_overwrite = true
  zone_id         = var.cloudflare_zone_id
  name            = "mondbasis.de"
  value           = hcloud_primary_ip.webserver_primary_ipv4.ip_address
  type            = "A"
  ttl             = var.cloudflare_default_ttl
}

resource "cloudflare_record" "mondbasis24de_ipv6" {
  allow_overwrite = true
  zone_id         = var.cloudflare_zone_id
  name            = "mondbasis.de"
  value           = "${hcloud_primary_ip.webserver_primary_ipv6.ip_address}1"
  type            = "AAAA"
  ttl             = var.cloudflare_default_ttl
}

resource "cloudflare_record" "wildcard_mondbasis24de_ipv4" {
  allow_overwrite = true
  zone_id         = var.cloudflare_zone_id
  name            = "*.mondbasis.de"
  value           = hcloud_primary_ip.webserver_primary_ipv4.ip_address
  type            = "A"
  ttl             = var.cloudflare_default_ttl
}

resource "cloudflare_record" "wildcard_mondbasis24de_ipv6" {
  allow_overwrite = true
  zone_id         = var.cloudflare_zone_id
  name            = "*.mondbasis.de"
  value           = "${hcloud_primary_ip.webserver_primary_ipv6.ip_address}1"
  type            = "AAAA"
  ttl             = var.cloudflare_default_ttl
}
